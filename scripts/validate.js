/**
 * Created by piotr on 19.01.15.
 */

function validateForm() {

    var postcode = document.forms["form"]["postcode"].value;
    if (!postal_code_validate(postcode)) {
        alert("Kod pocztowy nie jest poprawny");
        return false;
    }

    var mailaddr = document.forms["form"]["mailaddr"].value;
    if (!email_validate(mailaddr)) {
        alert("Adres email jest niepoprawny");
        return false;
    }

    var phoneno = document.forms["form"]["phoneno"].value;
    if (!phone_validate(phoneno)) {
        alert("Numer telefonu jest niepoprawny");
        return false;
    }

    return true;

}


function postal_code_validate(src)
{
    var regex = /^[0-9]{2}\-[0-9]{3}$/;
    return regex.test(src);
}

function email_validate(src)
{
    var regex = /^[a-zA-Z0-9._-]+@([a-zA-Z0-9.-]+\.)+[a-zA-Z0-9.-]{2,4}$/;
    return regex.test(src);
}

function phone_validate(src)
{
    var regex = /^[0-9]{9}$/;
    return regex.test(src);
}

function postal_code_style(elem)
{
    if (postal_code_validate(elem.value))
    {
        elem.style.border = "2px solid green";
    }
    else
    {
        elem.style.border = "2px solid red";
    }
}

function phone_style(elem)
{
    if (phone_validate(elem.value))
    {
        elem.style.border = "2px solid green";
    }
    else
    {
        elem.style.border = "2px solid red";
    }
}

function mail_style(elem)
{
    if (email_validate(elem.value))
    {
        elem.style.border = "2px solid green";
    }
    else
    {
        elem.style.border = "2px solid red";
    }
}
