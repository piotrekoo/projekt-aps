/**
 * Created by piotr on 19.01.15.
 */

function getDateTime() {
    var now     = new Date();
    var year    = now.getFullYear();
    var month   = now.getMonth()+1;
    var day     = now.getDate();
    var hour    = now.getHours();
    var minute  = now.getMinutes();

    if (month.toString().length == 1) {
        month = '0' + month;
    }

    if (day.toString().length == 1) {
        day = '0' + day;
    }

    if (hour.toString().length == 1) {
        hour = '0' + hour;
    }

    if (minute.toString().length == 1) {
        minute = '0' + minute;
    }

    return day.toString() + '.' + month + '.' + year + ' ' + hour + ':' + minute;
}


var div = document.getElementById('datetime');
div.innerHTML = getDateTime();